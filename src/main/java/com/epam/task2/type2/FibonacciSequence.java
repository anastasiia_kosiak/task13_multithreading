package com.epam.task2.type2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciSequence {
    private final int sizeOfSet;
    private int firstNumberOfFibonacciSequence;
    private int secondNumberOfFibonacciSequence;
    private int resultedNumberOfFibonacciSequence;
    private static Logger logger = LogManager.getLogger();

    public FibonacciSequence(final int sizeOfSet) {
        this.sizeOfSet = sizeOfSet;
        this.firstNumberOfFibonacciSequence = 0;
        this.secondNumberOfFibonacciSequence = 1;

    }

    public void executor() {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 2; i++) {
            executorService.submit(() -> {
                int i1 = 0;

                while (i1++ < sizeOfSet) {

                    resultedNumberOfFibonacciSequence = firstNumberOfFibonacciSequence
                            + secondNumberOfFibonacciSequence;
                    logger.warn("№" + i1 + " Fib number: " + resultedNumberOfFibonacciSequence);

                    firstNumberOfFibonacciSequence = secondNumberOfFibonacciSequence;
                    secondNumberOfFibonacciSequence = resultedNumberOfFibonacciSequence;
                }
            });
            executorService.shutdown();
        }
    }
}
