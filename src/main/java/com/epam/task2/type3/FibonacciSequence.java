package com.epam.task2.type3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciSequence implements Runnable{

    private final int sizeOfSet;
    private int firstNumberOfFibonacciSequence;
    private int secondNumberOfFibonacciSequence;
    private int resultedNumberOfFibonacciSequence;
    private int timeOfSleep;
    private static Logger logger = LogManager.getLogger();

    public FibonacciSequence(final int sizeOfSet, int timeOfSleep) {
        this.sizeOfSet = sizeOfSet;
        this.firstNumberOfFibonacciSequence = 0;
        this.secondNumberOfFibonacciSequence = 1;
        this.timeOfSleep = timeOfSleep;
    }

    @Override
    public void run() {
        int i = 0;

        while (i++ < sizeOfSet) {
            try {

                resultedNumberOfFibonacciSequence = firstNumberOfFibonacciSequence
                        + secondNumberOfFibonacciSequence;
                logger.warn("№" + i + " Fib number: " + resultedNumberOfFibonacciSequence);

                firstNumberOfFibonacciSequence = secondNumberOfFibonacciSequence;
                secondNumberOfFibonacciSequence = resultedNumberOfFibonacciSequence;
                Thread.sleep(timeOfSleep);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
