package com.epam.task2.type3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Application {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        FibonacciSequence fibonacciNumbers = new FibonacciSequence(10, 1000);
        Thread fibonacciSequence = new Thread(fibonacciNumbers, "Fib numbers");
        fibonacciSequence.start();
        FibonacciSequence fibonacciNumbers1 = new FibonacciSequence(15, 2000);
        Thread fibonacciSequence1 = new Thread(fibonacciNumbers1, "Fib numbers1");
        fibonacciSequence1.start();
        logger.warn(fibonacciSequence.getName() + " started");
        logger.warn(fibonacciSequence1.getName() + " started");
    }
}
