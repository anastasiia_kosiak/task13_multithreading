package com.epam.task2.type1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class FibonacciSequence implements Callable<Integer> {
    private final int sizeOfSet;
    private int firstNumberOfFibonacciSequence;
    private int secondNumberOfFibonacciSequence;
    private int resultedNumberOfFibonacciSequence;
    private List<Integer> storageForFibonacciNumbers;

    public FibonacciSequence(final int sizeOfSet) {
        this.sizeOfSet = sizeOfSet;
        this.firstNumberOfFibonacciSequence = 0;
        this.secondNumberOfFibonacciSequence = 1;

    }

    public void calculateFibonacciNumbers() {
        storageForFibonacciNumbers = new ArrayList<>();
        firstNumberOfFibonacciSequence = 0;
        secondNumberOfFibonacciSequence = 1;
        for (int i = 0; i < sizeOfSet; i++) {
            resultedNumberOfFibonacciSequence = firstNumberOfFibonacciSequence
                    + secondNumberOfFibonacciSequence;

            storageForFibonacciNumbers.add(resultedNumberOfFibonacciSequence);

            firstNumberOfFibonacciSequence = secondNumberOfFibonacciSequence;
            secondNumberOfFibonacciSequence = resultedNumberOfFibonacciSequence;
        }
    }


    @Override
    public Integer call() {
        calculateFibonacciNumbers();
        return storageForFibonacciNumbers.stream().
                reduce(0, Integer::sum);
    }
}
