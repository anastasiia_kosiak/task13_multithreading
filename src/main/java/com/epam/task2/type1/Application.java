package com.epam.task2.type1;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(2);
        List<Future<Integer>> list = new ArrayList<>();
        Callable<Integer> callable = new FibonacciSequence(5);
        for (int i = 0; i < 2; i++) {
            Future<Integer> future = executor.submit(callable);
            list.add(future);
        }
        for (Future<Integer> fut : list) {
            try {
                logger.warn("Sum = " + fut.get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        executor.shutdown();
    }

}
