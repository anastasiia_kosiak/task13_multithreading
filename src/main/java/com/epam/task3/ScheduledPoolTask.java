package com.epam.task3;

import java.time.LocalTime;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ScheduledPoolTask {
    private static Scanner scanner = new Scanner(System.in);
    private static Logger logger = LogManager.getLogger();
    private static Random random = new Random();

    public static void main(String[] args) throws InterruptedException {
        logger.warn("Print how much threads you want: ");
        int threadsNumber = scanner.nextInt();
        ScheduledExecutorService pool = Executors.newScheduledThreadPool(threadsNumber);
        for (int i = 1; i <= 5; i++) {
            int randomInteger = random.nextInt((5 - 1) + 1);
            pool.schedule(ScheduledPoolTask::task, randomInteger, TimeUnit.SECONDS);
        }
        pool.shutdown();
    }

    private static void task() {
        final LocalTime now = LocalTime.now();
        if (now.getSecond() % 10 == 0) {
            throw new IllegalStateException();
        }
        System.out.format("%s: %s%n", now, Thread.currentThread().getName());
    }
}
