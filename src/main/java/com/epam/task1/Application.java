package com.epam.task1;

import java.time.LocalDateTime;

public class Application {
    public static void main(String[] args) {

        PingPongGame game = new PingPongGame();
        System.out.println(LocalDateTime.now());
        game.show();
        System.out.println(LocalDateTime.now());
    }
}
