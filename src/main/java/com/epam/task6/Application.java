package com.epam.task6;

import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        final LockObject lockObject = new LockObject();
        new Thread(() -> lockObject.lockStart()).start();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        lockObject.setNumber();
        logger.warn("End");
    }
}
