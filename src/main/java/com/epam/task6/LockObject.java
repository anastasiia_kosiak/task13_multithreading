package com.epam.task6;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LockObject {
    private static Logger logger = LogManager.getLogger();
    private int number;
    private ReentrantLock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void lockStart() {
        try {
            logger.warn("Start lock: ");
            lock.lock();
            while (number <= 0) {
                condition.await();
            }
            logger.warn("Number is: " + number);
            logger.warn("Lock is closed");
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void setNumber() {
        try {
            lock.lock();
            logger.warn("Init number: ");
            number = new Scanner(System.in).nextInt();

        } finally {
            condition.signal();
            lock.unlock();
        }
    }
}
