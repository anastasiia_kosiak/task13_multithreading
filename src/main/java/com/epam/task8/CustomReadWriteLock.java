package com.epam.task8;

public class CustomReadWriteLock {
    private static final int WRITE_LOCKED = -1;
    private static final int FREE = 0;

    private int numberOfReaders;
    private Thread currentWriteLockOwner;

    CustomReadWriteLock() {
        numberOfReaders = FREE;
    }

    public synchronized void readLock() throws InterruptedException {
        while (numberOfReaders == WRITE_LOCKED) {
            wait();
        }
        numberOfReaders++;
    }

    public synchronized void readUnlock() {
        if (numberOfReaders <= 0) {
            throw new IllegalMonitorStateException();
        }
        numberOfReaders--;
        if (numberOfReaders == FREE) {
            notifyAll();
        }
    }

    public synchronized void writeLock() throws InterruptedException {
        while (numberOfReaders != FREE) {
            wait();
        }
        numberOfReaders = WRITE_LOCKED;
        currentWriteLockOwner = Thread.currentThread();
    }

    public synchronized void writeUnlock() {
        if (numberOfReaders != WRITE_LOCKED || currentWriteLockOwner != Thread.currentThread()) {

            throw new IllegalMonitorStateException();
        }
        numberOfReaders = FREE;
        currentWriteLockOwner = null;
        notifyAll();
    }
}
