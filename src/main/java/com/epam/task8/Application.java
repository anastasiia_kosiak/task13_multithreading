package com.epam.task8;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger();

    public static void main(String[] args) {
        CustomReadWriteLock lock = new CustomReadWriteLock();
        ExecutorService service = Executors.newFixedThreadPool(4);

        for (int i = 0; i < 4; i++) {
            service.submit(new Thread(new Student((i + 1), lock)));
        }

        try {
            TimeUnit.SECONDS.sleep(2);
            service.shutdownNow();
            logger.warn("Process is stopped");

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }
}
