package com.epam.task8;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Student implements Runnable {

    private static final double WRITE_PROB = 0.5;
    private static final Random rand = new Random();
    private final CustomReadWriteLock theLock;
    private final int ID;
    private static Logger logger = LogManager.getLogger();

    public Student(int ID, CustomReadWriteLock lock) {
        this.ID = ID;
        theLock = lock;
    }

    public void run() {
        logger.warn("Student: " + ID + " Started thread");
        while (!Thread.currentThread().isInterrupted()) {
            double r = rand.nextDouble();
            if (r <= WRITE_PROB) {
                write();
            } else {
                read();
            }
        }
    }

    private void read() {
        try {
            theLock.readLock();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        try {
            TimeUnit.MILLISECONDS.sleep(2);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            theLock.readUnlock();
        }
        logger.warn("Student: " + ID + " Finished reading.");

    }

    private void write() {
        try {
            theLock.writeLock();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        try {
            TimeUnit.MILLISECONDS.sleep(2);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            theLock.writeUnlock();
        }
        logger.warn("Student: " + ID + " Finished writing.");
    }
}
