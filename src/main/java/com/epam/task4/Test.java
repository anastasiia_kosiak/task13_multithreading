package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Test extends  Thread{
    private SyncObject testObj;
    private static Logger logger = LogManager.getLogger();

    Test(SyncObject testObj) {
        this.testObj = testObj;
    }

    @Override
    public void run() {
        logger.warn(Thread.currentThread().getName());
        testObj.getLine();
    }
}
