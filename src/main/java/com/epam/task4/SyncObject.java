package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SyncObject {
    private static Logger logger = LogManager.getLogger();

    synchronized void getLine() {
        for (int i = 0; i < 3; i++) {
            logger.warn(i);
            try {
                Thread.sleep(400);
            } catch (Exception e) {
                logger.warn(e);
            }
        }
    }
}
