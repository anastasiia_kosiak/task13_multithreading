package com.epam.task4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SyncTest extends Thread{

    private final SyncObject testObj;
    private static Logger logger = LogManager.getLogger();

    SyncTest(SyncObject testObj) {
        this.testObj = testObj;
    }

    @Override
    public void run() {
        synchronized (testObj) {
            logger.warn(Thread.currentThread().getName());
            testObj.getLine();

        }
    }
}
