package com.epam.task4;

public class Application {
    public static void main(String[] args) {
        SyncObject obj = new SyncObject();
        SyncObject obj1 = new SyncObject();
        SyncObject obj2 = new SyncObject();

        Test testPrint1 = new Test(obj);
        Test testPrint2 = new Test(obj);
        Test testPrint3 = new Test(obj);

        testPrint1.start();
        testPrint2.start();
        testPrint3.start();
    }
}
