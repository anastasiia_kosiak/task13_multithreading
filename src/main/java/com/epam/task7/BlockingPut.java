package com.epam.task7;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BlockingPut {
    private BlockingQueue<Integer> blockingQueue;
    private int count;
    private int size;
    private static boolean flag;
    private Random random;
    private static Logger logger = LogManager.getLogger();

    BlockingPut(BlockingQueue<Integer> blockingQueue, int size) {
        this.blockingQueue = blockingQueue;
        this.size = size;
        this.count = 0;
        random = new Random();
    }

    public void run() {
        try {
            while (!flag) {
                Integer number = random.nextInt(50);
                blockingQueue.put(number);
                logger.debug("Put thread: " + number);
                count++;
                if (count == size) {
                    logger.debug("Put thread stopped");
                    flag = true;
                }
                TimeUnit.SECONDS.sleep(2);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
