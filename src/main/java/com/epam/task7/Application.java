package com.epam.task7;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class Application {
    public static void main(String[] args) {
        BlockingQueue<Integer> blockingQueue = new LinkedBlockingDeque<>();

        BlockingPut blockingQueuePut = new BlockingPut(blockingQueue, 5);
        BlockingGet blockingQueueGet = new BlockingGet(blockingQueue, 5);

        blockingQueuePut.run();
        blockingQueueGet.run();
    }
}
