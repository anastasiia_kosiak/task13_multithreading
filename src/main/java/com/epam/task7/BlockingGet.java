package com.epam.task7;

import java.util.concurrent.BlockingQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BlockingGet {
    private BlockingQueue<Integer> blockingQueue;
    private int size;
    private int count;
    private static boolean flag;
    private static Logger logger = LogManager.getLogger();

    BlockingGet(BlockingQueue<Integer> blockingQueue, int size) {
        this.blockingQueue = blockingQueue;
        this.count = 0;
        this.size = size;
    }

    public void run() {
        try {
            while (!flag) {
                Integer number = blockingQueue.take();
                logger.warn("Get process: " + number);
                count++;
                if (count == size) {
                    logger.warn("Get stopped");
                    flag = true;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
