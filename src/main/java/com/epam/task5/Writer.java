package com.epam.task5;

import java.io.DataOutputStream;
import java.io.OutputStream;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Writer extends Thread {

    private DataOutputStream out;
    private static Random random = new Random();
    private static Logger logger = LogManager.getLogger();
    private static int count = 0;
    private boolean flag = true;

    public Writer(OutputStream os) {
        out = new DataOutputStream(os);
    }

    public void run() {
        while (flag) {
            try {
                double num = random.nextDouble();
                count++;
                logger.warn("WriteClass thread: " + num);
                out.writeDouble(num);
                out.flush();
                TimeUnit.SECONDS.sleep(2);
                if (count == 5) {
                    flag = false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}