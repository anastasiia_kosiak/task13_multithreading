package com.epam.task5;

import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        try {
            PipedOutputStream pout1 = new PipedOutputStream();
            PipedInputStream pin1 = new PipedInputStream(pout1);

            Writer writeClass = new Writer(pout1);
            Reader readClass = new Reader(pin1);

            writeClass.start();
            readClass.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
