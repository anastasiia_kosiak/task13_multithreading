package com.epam.task5;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Reader extends Thread {

    private DataInputStream in;
    private double total = 0;
    private int count = 0;
    private static Logger logger = LogManager.getLogger();
    private boolean flag = true;

    public Reader(InputStream is) {
        in = new DataInputStream(is);
    }

    public void run() {
        while (flag) {
            try {
                double x = in.readDouble();
                total += x;
                count++;
                logger.warn("ReadClass thread: " + x);
                logger.warn("Sum = " + total);
                if (count == 5) {
                    flag = false;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
